![logo](images/logo.png)

# About the Team
At PetroPower, we're building exciting new products for the oil and gas
industry. It is our mission is to unlock the full potential of every well, in
every field, through visibility, knowledge, and automation. 

Our team has over 35 years of combined experience and every day presents the
opportunity to excel in a fast-paced, dynamic role that will expand in tandem
with your unique skill set.

The following will appeal to the ideal candidate:

  * You’re looking for a cultural ‘fit’ rather than just a job
  * Passion, an outgoing personality and self-motivation define you
  * You like to be part of a team, but also work well independently without supervision
  * Customer satisfaction is important to you, but you remain true to your values
  * You love collaborating with others, reading and learning like you’ve never left school
  * You are curious, creative, biased toward action and love solving problems
  * It is in your nature to do less, but do it well, and always deliver what you promise
  * You don’t assume that you are the best and have humility to learn from others
  * You see the world as a set of mathematical equations

If this describes you, consider joining our team!

## About our Technology
Our team leverages the best tools available to solve problems and consistently
deliver value over a wide range of environments and disciplines:

We're always on the lookout for candidates with skills and expertise in any of
the following:

  * Sensing and data acquisition
  * Data filtering
  * Embedded systems
  * Edge computing on constrained systems
  * Local and wide area wireless technologies
  * Device fleet management
  * Cloud infrastructure
  * Data analytics and machine learning
  * Relational, document, and time-series databases
  * Data modeling and API design
  * Computational physics and statistics
  * Containerization and orchestration
  * Routing and networking

## Application Process
If you find our roles and team interesting, we’d love to hear from you! Send your resume to `jobs@petropower.com` and if we think you're a good fit, we will reach out!

The Process:

  1. Submit resume
  2. Phone interview
  3. Submit _one_ [homework problem](homework/) solution to `jobs@petropower.com`
  4. Engineering Interviews
  5. If all goes well, welcome to the team!
