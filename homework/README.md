# Homework

## Purpose
Evaluating technical ability is an essential aspect of hiring engineers, but
writing a sort algorithm on a whiteboard is not very indicative of the work
we do on a daily basis.

These homework problems are designed and formatted to be a much closer fit to
the real world issues we identify and tackle every day.

Just like any member of our team, you are free to solve these problems in your
own time and with the tools and resources you are most familiar with.

## Assignments
See the following list for available homework assignments. Feel free to choose
the assignment best suited to your skills:

  * [proto](proto/)
  * [slcsp](slcsp/)

## Testing
Automated testing is an essential component of PetroPower's CI/CD process.
Please include appropriate automated tests alongside your solution.

Testing Tips:

  * We're familiar with most testing tools and frameworks. Feel free to use what you see fit.
  * Be efficient with your coverage. You do not need 100% coverage.
  * Be efficient with your test cases. You do not need to enumerate every far-fetched edge case.

## Evaluation Process
All problems have a standardized grading rubric and will be evaluated
independently by multiple engineers. We are a small team, but wherever
possible we attempt to do the evaluations blind in order to remove as much bias
from the process as we can.

Please do not include your name, email address, or other identifiers in your
solution. Your submission will be given a random identifier when it is graded.

## Tips
A few pointers:

  * We want to see how you approach real world problems -- from understanding to solution.
  * There are no hidden objectives or meanings within the problem descriptions.
  * Aim for a clear and concise MVP for your solution.
  * Feel free to use your language and tools of choice. 
  * Submit your solution as one would to a peer for code review.
  * Don't feel pressure to demonstrate every skill you have. There is no need to over-engineer your solution.

## Questions
We believe good developers ask questions sooner rather than later.
Feel free to contact us with any questions about the problem at:

`homework@petropower.com`

We will get back to you within 24 hours.

## Submitting
Once your solution is complete, please bundle your source, tests, data, and
comments using your preferred archive tool and email this file and your resume
to:

`jobs@petropower.com`

## Credit
These problems are sourced from the excellent set of homework problems
published by the Ad Hoc Team. You can find them [here](https://homework.adhoc.team/).
