# Senior UI/UX Engineer
As a senior UI/UX engineer you will be responsible for designing, deploying, and maintaining our web front-end for an exciting new product.  You will collaborate with our engineering and marketing teams to integrate domain knowledge and business logic into a graphical interface that will excite and delight our customers.

You will be required to execute and contribute from conception to final product, exercising good organizational skills and attention to detail.

## Role Responsibilities
  * Heavy application of web front-end design and development skills to build and deploy a critical component of an exciting new product
  * Develop user interactions and workflows through our webpage
  * Collaborate with engineering, espically with technical lead and back-end developers, to define schema and view model for back-end resources
  * Ensure good web page performance in a variety of environments
  * Write and maintain unit tests to ensure consistent product quality and customer experience, conforming to our high quality standards
  * Work closely with all business units and engineering teams to develop the long term strategy for evolution of the customer interface and product experience

## Qualifications
  * BS in Computer Science or clearly demonstrated relevant experience with public portfolio and references
  * 3+ years of professional software development experience
  * Skilled with general software principles, patterns, and practices
  * Extensive understanding and experience with agile software development concepts and processes
  * Understanding of CICD with the ability to build and maintain CICD pipelines is a must
  * Proficiency with fundamental front end languages such as HTML, CSS and JavaScript
  * Proficienty in at least one of three top JavaScript frameworks (Angular JS, React, Vue)
  * Familiarity with API Authentication (JWT/Basic) and consuming GraphQL API's & usual REST resources
  * Familiarity with server side languages such as Python, Ruby, Java, PHP and .Net
  * Expierence with front-end application deployment
  * Must be proactive, demonstrate initiative, and be a logical thinker
  * Team player with experience working in a collaborative environment

## How to Stand Out
While not essential, experience with any of the following will set you ahead:

  * Proficiency with time-series database technology such as Prometheus, influxDB
  * Message streaming with MQTT or Kafka
  * Docker and container based virtualization
  * Cloud integrations (AWS Primarily)
  * Atlassian's suite of developer tools (Jira, Bitbucket, Bamboo, etc.)

## Salary Range
$65,000 - $90,000

## How to Apply
See our [application process](../README.md) section
