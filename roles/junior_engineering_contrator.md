# Junior Engineering Contrator

## Role Responsibilities
Work with project leads to accelerate the completion of:

  * Running and automating tests
  * Discovery and root cause analysis for defects
  * Task automation
  * Documentation
  * Data collection

## Qualifications
  * BS in engineering, computer science or equivalent relevant experience
  * Must be proactive, demonstrate initiative, and be a logical thinker
  * Team player with a collaborative attitude

## How to Stand Out
While not essential, experience with any of the following will set you ahead:

  * Docker and container based virtualization
  * AWS services like RDS, ECS, and EC2
  * Data modeling and API design with Ruby, Go, Python, or Node
  * Cryptographic trust and security
  * Atlassian's suite of developer tools (Jira, Bitbucket, Bamboo, etc.)

## How to Apply
See our [application process](../README.md) section