# Senior Data Scientist
As Senior Data Scientist you will be responsible for applying your data analysis and machine learning expertise to solve diverse and novel problems in the oil and gas industry.

## Role Responsibilities
  * Apply your background in data analysis to continually uncover hidden insights from multiple data sources
  * Effectively communicate data insights to diverse audiences including executives, engineers, and customers using appropriate visual tools and language 
  * Work closely with all business units and engineering teams to develop the long term strategy for PetroPower's data platform architecture

## Qualifications
  * BS/MS in engineering, mathematics, physics or related field. Equivalent relevant experience will also be considered
  * 3+ years of professional software development experience
  * Demonstrated experience solving real-world problems using data analysis techniques
  * Skilled with general software principles, patterns, and practices
  * Skilled with scientific computing tools and languages
  * Extensive understanding and experience with agile software development concepts and processes
  * Must be proactive, demonstrate initiative, and be a logical thinker
  * Team player with experience working in a collaborative environment

## How to Stand Out
While not essential, experience with any of the following will set you ahead:

  * Kalman filters and modeling
  * Data classification and predictions (ANN, SVM, etc.)
  * Distributed data processing (Spark, Hadoop, etc.)
  * Docker and container based virtualization
  * Message streaming with MQTT or Kafka
  * Atlassian's suite of developer tools (Jira, Bitbucket, Bamboo, etc.)

## Salary Range
$90,000 - $130,000

## How to Apply
See our [application process](../README.md) section
