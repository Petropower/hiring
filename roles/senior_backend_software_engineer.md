# Senior Backend Software Engineer

## Role Responsibilities
  * Work with project stakeholders to capture domain models
  * Implement models using relational, object, and document based storage.
  * Develop and execute plans to ensure data integrity as model evolves
  * Create intuitive and secure APIs for both the web and internal applications
  * Secure user authentication and authorization

## Qualifications
  * BS/MS in engineering, computer science or equivalent relevant experience
  * Experience developing user-facing web applications and APIs
  * Experience using RDBMS systems like MySQL or PostgreSQL to model and store data
  * Experience in automated testing and CI/CD
  * Extensive understanding of agile software development concepts and processes
  * Must be proactive, demonstrate initiative, and be a logical thinker
  * Team player with experience working in a collaborative environment

## How to Stand Out
While not essential, experience with any of the following will set you ahead:

  * API documentation tools like Swagger and RAML
  * GraphQL and other alternatives
  * Automation using tools like Ansible and Terraform
  * Docker and container based virtualization
  * AWS services like RDS, ECS, and EC2
  * Message streaming with MQTT or Kafka
  * Data modeling and API design with Ruby, Go, Python, or Node
  * Cryptographic trust and security
  * Atlassian's suite of developer tools (Jira, Bitbucket, Bamboo, etc.)

## How to Apply
See our [application process](../README.md) section